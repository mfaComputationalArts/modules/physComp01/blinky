/**
 * with-lasers / lxinspc
 * Nathan Adams 2019
 * 
 * MFA Computational Arts / Physical Computing 1
 * 
 * Micro-brief 2: Blinky, blink an LED in a creative way
 * 
 * Got carried away, and found a strip of NeoPixels in my electronics boxes. So made a 10x10 grid with them, which can be basis for future for Micro-briefs.
 * 
 * In this example, there is
 * 
 * 1) A test function to check we can address the pixel grid correctly - this allows for the zig / zag construcction of it, where rows 1/3/5/7/9 are reveresed 
 * 2) The main interesting(?) function blinkInteresting
 * 
 * BlinkInteresting, sets a random background color (that is, the color of all the cells in the grid), it then sets a number of highlight pixels (random each time) of a random color
 * on the grid. A random delay is set, before the random figure is shown again
 * 
 * Example video: https://vimeo.com/366117655
 * 
 * Things to do next…
 * 
 * 1) Fade background changed
 * 2) add / remove highlight pixels at different times
 * 
 */

//We use the AdaFruit NeoPixel Library to control the grid we have made
#include <Adafruit_NeoPixel.h>

//CONSTANTS

//NeoPixel Grid Dimensions
#define PIX_X 10
#define PIX_Y 10
//Offset - the voltage level convertor / power supply board, has one neo pixel used as a test indicator, so striup begins at 1 - not 0
#define OFFSET 1

//NEOPIXEL SETUP
#define LED_PIN 6
#define LED_COUNT 101     // 10 x 10 grid, plus the extra test neopixel on the level convertor / power board

Adafruit_NeoPixel grid(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);   //Setup the NeoPixel object we will use to access to display


void setup() {
  Serial.begin(9600);
  grid.begin();
  grid.show();

  // if analog input pin 0 is unconnected, random analog
  // noise will cause the call to randomSeed() to generate
  // different seed numbers each time the sketch runs.
  // randomSeed() will then shuffle the random function.

  // from https://www.arduino.cc/reference/en/language/functions/random-numbers/random/
  
  randomSeed(analogRead(0));

}

void loop() {
  // put your main code here, to run repeatedly:
  //testAllSingleLight();

  blinkInteresting();

}

void testAllSingleLight() {
  //Test function, to make sure mapping to pixels works as epxetced - light each pixel indivusally, running 0>9 x, and 0>9y

  int pixel = coordsToPixelFine(PIX_X - 1, PIX_Y - 1);

  for (int y=0;y<PIX_Y;y++) {
    for (int x=0;x<PIX_X;x++) {
       grid.begin();                              //
       grid.setPixelColor(pixel,0,0,0);           //turn the last pixel off
       pixel = coordsToPixelFine(x,y);            //get new pixel
       grid.setPixelColor(pixel,255,255,255);     //turn on (WHITE)
       grid.show();                               //update the grid
       delay(50);                                // wait till we do this again
    }
  }

  
}

void blinkInteresting() {

  //Set a random number of pixels to set a color for
  int numPixels = random(20, 45);               //Random number of highlight pixels

  long fillVariation = random(-1500,1500);      //We will vary the background around a base hue of 20922 - remeber this needs to be long, int will get maxed out!

  grid.begin();                                 //Start and clear the grid
  grid.clear();
  grid.fill(grid.ColorHSV(20922+fillVariation,100,40),1,LED_COUNT);   //Fill with a background color based around 20922 +/- 1500 (maybe this needs to be more?)

  for (int i=0;i<numPixels;i++) {         
    long pixVariation = random(30000,40000);
    grid.setPixelColor(random(1,100),grid.ColorHSV(pixVariation,100,100));    //Set pixel color, based on the random Hue between 30000 and 40000
  }

  grid.show();


  int randomWait = random(250,750);           //Wait for a random period of time before we do this all over again
  delay(randomWait);

  
}




int coordsToPixelFine(int x, int y) {

  //Returns pixel to be illuminated based on X / Y coordiantes. This is a transform which handles the fact
  //that the neoPixel strip zig zags, couting up on y % 2 == 0 roes, and down on y % 2 != 0
  //This is the FINE version of it, where we can address each pixel individually - COARSE will allow for cell access, for example a 2x2 cell, giving 5x5 for a 10x10 grid

  //TO-DO function has been parametised to PIX_X / PIX_Y rather than 10x10 - not yet checked if data is correct for other grid sizes
  return (y % 2 == 0) ? x + (y * PIX_X) + OFFSET : ((y + 1) * PIX_X) - x - 1 + OFFSET;

  
}
